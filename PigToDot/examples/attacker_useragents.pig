-- taken from packetloop's packetpig on Github (claims to be open source)
-- https://github.com/packetloop/packetpig/tree/master/pig/examples

-- NOTE: removed the lines starting %DEFINE - the parser couldn't handle them
-- for some reason?

snort_alerts = LOAD '$pcap' USING com.packetloop.packetpig.loaders.pcap.detection.SnortLoader('$snortconfig') AS (
    ts:long,
    sig:chararray,
    priority:int,
    message:chararray,
    proto:chararray,
    src:chararray,
    sport:int,
    dst:chararray,
    dport:int
);

http_conversations = LOAD '$pcap' USING com.packetloop.packetpig.loaders.pcap.protocol.HTTPConversationLoader('$tcppath', '$field') AS (
    ts:long,
    src:chararray,
    sport:int,
    dst:chararray,
    dport:int,
    field:chararray
);

snort_http = JOIN
                snort_alerts BY (src, sport, dst, dport),
                http_conversations BY (src, sport, dst, dport);

useragent_frequency = GROUP snort_http BY field;
summary = FOREACH useragent_frequency GENERATE group, COUNT(snort_http) AS count;
summary = ORDER summary BY count DESC;

STORE summary INTO '$output/attacker_useragents';