package uk.org.nickmoore.pig2dot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Scanner;

import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.Tree;
import org.apache.pig.impl.PigContext;
import org.apache.pig.parser.QueryParserDriver;
import org.apache.pig.parser.QueryParserUtils;
import org.kohsuke.graphviz.Graph;
import org.kohsuke.graphviz.Node;
import org.kohsuke.graphviz.Style;

public class Pig2Dot {
	HashMap<String, Node> nodes = new HashMap<>(); 
	Graph queryGraph = new Graph();
	
	public Pig2Dot(File inputFile) throws FileNotFoundException {
		Scanner scanner = new Scanner(inputFile);
		parseTree(scanner.useDelimiter("\\Z").next());
		scanner.close();
	}
	
	public Graph getGraph() {
		return queryGraph;
	}
	
	private Node getNode(String label) {
		// is this necessary? Would using new Node().id(label) have the same 
		// effect? hashCode and equals aren't defined for Node, so I think so
		label = QueryParserUtils.removeQuotes(label);
		if(!nodes.containsKey(label)) {
			Node node = new Node().id(label);
			nodes.put(label, node);
		}
		return nodes.get(label);
	}

	/**
	 * This is a lot of nasty code to call three stupid functions from the Pig
	 * library that are (IMHO, needlessly) private (tokenize, parse and 
	 * expandMacro). Expanding them out resulted in coming across protected 
	 * instantiation methods that just aren't worth the time of day trying to
	 * fix. And before you ask, I don't have the time to start contributing to 
	 * an Apache project - GPL is hard enough frankly.
	 * @param query the full text of the Pig Latin query 
	 * @return a AST representing the query
	 */
	public Tree getParseTree(String query) {
		Tree tree = null;
		try {
			Class<?>[] tokenizeArgs = {String.class, String.class};
			QueryParserDriver driver = new QueryParserDriver(new PigContext(), 
					"", new HashMap<String, String>());
			Method method = driver.getClass().getDeclaredMethod("tokenize", 
					tokenizeArgs);
			method.setAccessible(true);
			CommonTokenStream tokenStream = (CommonTokenStream) method.invoke(
					driver, query, "testfile");
			Class<?>[] parseArgs = {CommonTokenStream.class};
	        method = driver.getClass().getDeclaredMethod("parse", parseArgs);
	        method.setAccessible(true);
	        Tree ast = (Tree) method.invoke( driver, tokenStream );
	        Class<?>[] expandArgs = {Tree.class};
	        method = driver.getClass().getDeclaredMethod("expandMacro", 
	        		expandArgs);
	        method.setAccessible(true);
	        tree = (Tree) method.invoke( driver, ast );
		}
		catch(Exception ex) {
			// WTF?
			ex.printStackTrace();
		}
        return tree;
	}
	
	private void parseTree(String input) {
		Tree query = getParseTree(input);
		for(int i = 0; i < query.getChildCount(); i++) {
			Tree line = query.getChild(i);
			if(line.getText().equalsIgnoreCase("STATEMENT")) {
				if(line.getChildCount() == 1) {
					Tree method = line.getChild(0);
					Style s = new Style().attr("label", method.getText());
					if(method.getText().equalsIgnoreCase("STORE")) {
						getGraph().edge(
								getNode(method.getChild(0).getText()), 
								getNode(method.getChild(1).getText()), s);
					}
				}
				else {
					Tree method = line.getChild(1);
					Style s = new Style().attr("label", method.getText());
					if(method.getText().equalsIgnoreCase("JOIN")) {
						for(int j = 0; j < method.getChildCount(); j++) {
							Tree item = method.getChild(j);
							if(item.getText().equalsIgnoreCase("JOIN_ITEM")) {
								getGraph().edge(
										getNode(item.getChild(0).getText()), 
										getNode(line.getChild(0).getText()), s);
							}
						}
					}
					else if(method.getText().equalsIgnoreCase("UNION")) {
						for(int j = 0; j < method.getChildCount(); j++) {
							getGraph().edge(
									getNode(method.getChild(j).getText()),
									getNode(line.getChild(0).getText()), s);
						}
					}
					else {
						getGraph().edge(
								getNode(method.getChild(0).getText()), 
								getNode(line.getChild(0).getText()), s);
					}
				}
			}
			else if(line.getText().equalsIgnoreCase("SPLIT")) {
				Style s = new Style().attr("label", line.getText());
				for(int j = 0; j < line.getChildCount(); j++) {
					Tree branch = line.getChild(j);
					if(branch.getText().equalsIgnoreCase("SPLIT_BRANCH") || 
							branch.getText().equalsIgnoreCase("OTHERWISE")) {
						getGraph().edge(
								getNode(line.getChild(0).getText()), 
								getNode(branch.getChild(0).getText()), 
								s);
					}
				}
			}
		}
	}

	/**
	 * @param args only one argument - the filename of the Pig Latin script
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException {
		Pig2Dot p2d = new Pig2Dot(new File(args[0]));
		p2d.getGraph().writeTo(System.out);
	}

}
